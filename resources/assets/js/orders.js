window.Vue = require('vue');

import bPagination from 'bootstrap-vue/es/components/pagination/pagination';
import dict from '../../lang/ua.json';

Vue.component('message-alert', require('./components/MessageAlert.vue'));
Vue.component('b-pagination', bPagination);
Vue.component('orders-filters', require('./components/OrdersFilters.vue'));
Vue.component('orders-index-table', require('./components/OrdersIndexTable.vue'));

const app = new Vue({
    el: '#app',

    data: {
        pagination: {
            perPage: 0,
            totalRows: 0,
            currentPage: 1
        },
        statuses: null,
        tracklists: null,
        printable: false,
        dict: dict
    },


    methods: {
        alert: function (message) {
            this.$refs.alert.show(message)
        },

        paginate: function (page) {
            let param = {
                name: 'page',
                value: page
            };
            this.query(param);
        },

        query: function (param) {
            this.$refs.orders.query(param);
        },

        print: function () {
            this.printable = true;

            this.$nextTick(function () {
                let table = this.$refs.orders.$el;

                let doc =
                    '<html>' +
                    '<head>' +
                    '<link href="/css/app.css" rel="stylesheet" type="text/css" media="print">' +
                    '<style type="text/css" media="print">@page { size: landscape; }</style>' +
                    '</head>' +
                    '<body onload="window.print();window.close()">' +
                    table.outerHTML +
                    '</body>' +
                    '</html>';

                let page = window.open('');
                page.document.write(doc);
                page.document.close();

                this.printable = false;
            });
        },

        setData: function(data) {
            this.pagination = {
                perPage:data.orders.per_page,
                totalRows:data.orders.total,
                currentPage:data.orders.current_page
            };
            this.tracklists = data.tracklists;
            this.statuses = data.statuses;
        },
    }
});