window.Vue = require('vue');

import bPagination from 'bootstrap-vue/es/components/pagination/pagination';
import dict from '../../lang/ua.json';

Vue.component('message-alert', require('./components/MessageAlert.vue'));
Vue.component('b-pagination', bPagination);
// Vue.component('orders-filters', require('./components/OrdersFilters.vue'));
Vue.component('items-table', require('./components/ItemsTable.vue'));

const app = new Vue({
    el: '#app',

    data: {
        pagination: {
            perPage: 0,
            totalRows: 0,
            currentPage: 1
        },
        statuses: null,
        dict: dict
    },


    methods: {
        alert: function (message) {
            this.$refs.alert.show(message)
        },

        paginate: function (page) {
            let param = {
                name: 'page',
                value: page
            };
            this.query(param);
        },

        query: function (param) {
            this.$refs.items.query(param);
        },

        setData: function(data) {
            this.pagination = {
                perPage:data.items.per_page,
                totalRows:data.items.total,
                currentPage:data.items.current_page
            };
            this.statuses = data.statuses;
        },
    }
});