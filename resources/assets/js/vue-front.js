require('./bootstrap');


window.Vue = require('vue');

new Vue({

    el: '#vue',

    data: {
        users: [],
        pagination: {
            total: 0,
            per_page: 0,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 1,
        checked_ids: [],
        formShow: false,
        messageText: ''
    },

    computed: {
        isActived: function () {
            return this.pagination.current_page;
        },
        pagesNumber: function () {
            if (!this.pagination.to) {
                return [];
            }
            var from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            var to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            var pagesArray = [];
            while (from <= to) {
                pagesArray.push(from);
                from++;
            }
            return pagesArray;
        }
    },



    methods : {

        getUsers: function(page) {
            var vue = this;
            axios.get('vue/users?page='+page)
                .then(function (response) {
                    vue.users = response.data.data.data;
                    vue.pagination = response.data.pagination;
                });
            },

        deleteThis: function(user) {
            if (confirm("Are you sure?")) {
                console.log('user ' + user.tel_id +' will be deleted.');

                var vue = this;
                axios.delete('users/delete/' + user.tel_id).then(function() {
                    vue.getUsers(1);
                    alert('Done!');
                });
            }
        },

        changePage: function (page) {
            this.pagination.current_page = page;
            this.getUsers(page);
        },
        unsubscribe: function() {
            console.log('Init unsubscribe fn');

            var vue = this;
            axios.post('users/unsubscribe', {
                'users': vue.checked_ids
            }).then(function() {
                vue.getUsers(1);
                alert('Done!');
            });
        },
        sendMessage: function() {
            console.log('Init sendMessage fn');
            
            if(this.messageText !== '') {
                console.log(this.messageText);
                console.log(this.checked_ids);
                var vue = this;
                if (this.checked_ids.length === 0) {
                    alert('Select something first!');
                    return false;
                }
                axios.post('message', {
                    'message': this.messageText,
                    'users': this.checked_ids
                }).then(function() {
                    alert('Done!');
                });
                vue.messageText = '';
                vue.formShow = !vue.formShow;
            }
        }



    },
    mounted : function(){
        this.getUsers(this.pagination.current_page);
    }

});
