require('./bootstrap');

$(document).ready(function() {
    $("#sidebarCollapse").on("click", function() {
        if ($("#sidebar").hasClass('active')) {
            document.cookie = "sidebar=";
        } else {
            document.cookie = "sidebar=hide";
        }
        $("#sidebar").toggleClass("active");
        $(this).toggleClass("active");
    });

    $(".phone").inputmask("(099) 999-99-99");
});