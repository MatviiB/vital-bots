<nav id="sidebar" @if(Cookie::get('sidebar') == 'hide') class="active" @endif>
    <div class="sidebar-header">
        <h3>CRM ({{ env('APP_ENV') }})</h3>
    </div>

    <ul class="list-unstyled components">
        {{--<p>Navigation</p>--}}

        <li @if(request()->is('orders*')) class="active" @endif>
            <a href="{{ route('orders.index') }}">@lang('Orders')</a>
        </li>
        <li @if(request()->is('tracklists*')) class="active" @endif>
            <a href="{{ route('tracklists.index') }}">@lang('Tracking')</a>
        </li>
        <li @if(request()->is('clients*')) class="active" @endif>
            <a href="{{ route('clients.index') }}">@lang('Clients')</a>
        </li>
        <li @if(request()->is('items*')) class="active" @endif>
            <a href="{{ route('items.index') }}">@lang('Items')</a>
        </li>
        @if(Auth::user()->isAdmin())
        <li @if(request()->is('factories*')) class="active" @endif>
            <a href="{{ route('factories.index') }}">@lang('Factories')</a>
        </li>
        @if(Auth::user()->isAdmin())
            <li @if(request()->is('reports*')) class="active" @endif>
                <a href="#reports" data-toggle="collapse" aria-expanded="false">@lang('Reports')</a>
                <ul class="collapse list-unstyled" id="reports">
                    <li @if(request()->is('reports/1')) class="active" @endif>
                        <a href="{{ route('reports.1') }}">@lang('Report 1')</a>
                    </li>
                    <li @if(request()->is('reports/2')) class="active" @endif>
                        <a href="{{ route('reports.2') }}">@lang('Report 2')</a>
                    </li>
                    <li @if(request()->is('reports/3')) class="active" @endif>
                        <a href="{{ route('reports.3') }}">@lang('Report 3')</a>
                    </li>
                </ul>
            </li>
        @endif
        <li @if(request()->is('texts*')) class="active" @endif>
            <a href="{{ route('texts.index') }}">@lang('Texts')</a>
        </li>
        <li @if(request()->is('users*')) class="active" @endif>
            <a href="{{ route('users.index') }}">@lang('Users')</a>
        </li>
        @endif
    </ul>
</nav>