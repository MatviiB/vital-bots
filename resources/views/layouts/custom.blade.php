<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('layouts.head')

<body>

<div class="wrapper" id="app">

    @include('layouts.sidebar')

    <div id="content" style="width: 100%">

        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
            <i class="glyphicon glyphicon-align-left"></i>
            <span></span>
        </button>

        @include('layouts.messages')

        @yield('content')

    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/inputmask.js') }}"></script>

@if(is_file('js/vue/' . request()->path() . '.js'))
    <script src="{{ asset('js/vue/'. request()->path() .'.js') }}"></script>
@endif

@yield('js')
</body>