<script>

    var client_discount = 0;

    $.each(document.getElementsByClassName("datepicker"), function() {
        $(this).datetimepicker({
            format: 'DD/MM/YYYY',
            useCurrent: false,
            locale: 'uk',
        });
    });

    var table = document.getElementById("items");

    var rowHtml = "{!! str_replace(["\r","\n"], "", \View::make('items.row', compact('factories'))->render()) !!}";

    function plus() {
        $('table tbody').append(rowHtml.replace(/ROW/g, table.rows.length));
    }

    function remove(id) {
        event.preventDefault();

        var index = document.getElementById("remove_" + id)
            .parentElement
            .parentElement
            .rowIndex;

        table.deleteRow(index);
        getTotal();
    }

    function getTotal() {
        var sum = 0;

        $.each(document.getElementsByClassName("price-add"), function() {
            sum += parseInt(this.value) || 0;
        });

        var checkbox = document.getElementById("accept_discount");

        if (checkbox.checked === true) {
            if ($('input[name="client[discount]"]').val() > 0) {
                client_discount = $('input[name="client[discount]"]').val();
            }
            sum -= parseInt(client_discount || 0);
            $('input[name="order[discount]"]').val(client_discount);
            $('input[name="client[discount]"]').val(0);
        } else {
            $('input[name="client[discount]"]').val(
                parseInt($('input[name="client[discount]"]').val()) +
                parseInt($('input[name="order[discount]"]').val() || 0)
            );
            $('input[name="order[discount]"]').val(0);
        }

        $('input[name="order[total]"]').val(sum);
    }

    function profit(id) {
        var profit = (parseInt($('input[name="items[' + id +'][price_out]"]').val()) - parseInt($('input[name="items[' + id +'][price_in]"]').val())) * 100 / parseInt($('input[name="items[' + id +'][price_in]"]').val());
        $('#items-' + id + '-profit').val(profit);
    }

    $(document).ready(function() {
        for (var i = 1; i < table.rows.length; i++) {
            profit(i);
        }
    });
</script>