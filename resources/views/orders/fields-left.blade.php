<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Conversion')</label>
    <div class="col-sm-8">
        <select name="order[conversion]" class="form-control">
            @if(isset($order))
                <option value="{{ $order->conversion }}">@lang($order->conversion_status_verbal)</option>
            @endif
            @foreach(App\Models\Order::conversionStatuses() as $value => $status)
                <option value="{{ $value }}">@lang($status)</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Created')</label>
    <div class="col-sm-8">
        <div class='input-group date datepicker'>
            <input type='text' class="form-control" name="order[date_created]" required
                @if(isset($order)) value="{{ \Carbon\Carbon::parse($order->date_created)->format('d/m/Y') }}" @else value="" @endif>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Date ready')</label>
    <div class="col-sm-8">
        <div class='input-group date datepicker'>
            <input type='text' class="form-control" name="order[date_ready]" required
                   @if(isset($order)) value="{{ \Carbon\Carbon::parse($order->date_ready)->format('d/m/Y') }}" @else value="" @endif>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Note')</label>
    <div class="col-sm-8">
        <textarea class="form-control" rows="3" name="order[note]">{{ $order->note ?? null }}</textarea>
    </div>
</div>