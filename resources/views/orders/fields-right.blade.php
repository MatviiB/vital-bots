<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Delivery Price')</label>
    <div class="col-sm-8">
        <input type="number" class="form-control price-add" name="order[delivery]" value="{{ $order->delivery ?? null }}" oninput="getTotal()">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Prepayment')</label>
    <div class="col-sm-8">
        <input type="number" class="form-control pull-left" name="order[prepayment]" style="width: 30%; margin-right: 5px;" value="{{ $order->prepayment ?? null}}">
        <select name="order[prepayment_type]" class="form-control pull-left" style="width: 30%;">
            @if(isset($order))
                <option value="{{ $order->prepayment_type }}">@lang("$order->prepayment_type_verbal")</option>
            @endif
            @foreach(App\Models\Order::prepaymentTypes() as $value => $type)
                <option value="{{ $value }}">@lang("$type")</option>
            @endforeach
        </select>
        <div class="checkbox pull-right text-right" style="width: 35%">
            <label>
                <input
                        type="checkbox"
                        name="order[prepayment_status]"
                        value="1"
                    @if(isset($order) and $order->prepayment_status) checked @endif
                >@lang('Passed')
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-4 col-sm-8">
        <div class="checkbox">
            <label>
                <input
                        type="checkbox"
                        id="accept_discount"
                        name="accept_discount"
                        @if(isset($order) && $order->discount) checked @endif
                        oninput="getTotal()"
                > @lang('Accept Discount')
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Total')</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="order[total]" readonly
               value="{{ $order->total ?? null }}">
    </div>
</div>

{{--<div class="form-group">
    <label class="col-sm-4 control-label">Bonus, %</label>
    <div class="col-sm-3">
        <input type="number" min="0" max="100" class="form-control" name="order[bonus_percentage]" value="{{ $order->bonus_percentage ?? null }}" onclick="calcDicount()">
    </div>
    <div class="col-sm-5">
        <input type="text" readonly id="bonus" class="form-control" name="order[bonus]" value="{{ $order->bonus ?? null }}">
    </div>
</div>--}}


<input type="hidden" name="order[discount]" value="{{ $order->discount ?? null }}">