@extends('layouts.custom')

@section('content')
    <div class="col-md-12">
        <h2>Create new Order</h2>
    </div>

    <form class="form-horizontal" action="{{ route('order.store') }}" method="POST">

        {{ csrf_field() }}

        <input type="hidden" name="order[client_id]" value="{{ $client->id ?? null }}">
        <input type="hidden" name="order[user_id]" value="{{ Auth::id() }}">
        <input type="hidden" name="order[status]" value="{{ \App\Models\Order::STATUS_VERIFY }}">

        <div class="col-md-3">
            @include('clients.fields')
        </div>

        <div class="col-md-3">
            @include('clients.addresses')
        </div>

        <div class="col-md-3">
            @include('orders.fields-left')
        </div>

        <div class="col-md-3">
            @include('orders.fields-right')
        </div>

        <div class="col-md-12">
            @include('items.table')
        </div>

        <div class="col-md-6 col-md-offset-6">
            <div class="pull-right">
                <button type="submit" class="btn btn-default">Create</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    @include('orders.js')
@endsection