@extends('layouts.custom')

@section('content')

    <div class="col-md-12">
        <h2>Edit Order</h2>
    </div>

    <form class="form-horizontal" action="{{ route('order.update', $order->id) }}" method="POST">

        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <input type="hidden" name="client_id" value="{{ $client->id }}">
        <input type="hidden" name="user_id" value="{{ Auth::id() }}">

        <div class="col-md-3">
            @include('clients.fields')
        </div>

        <div class="col-md-3">
            @include('clients.addresses')
        </div>

        <div class="col-md-3">
            @include('orders.fields-left')
        </div>

        <div class="col-md-3">
            @include('orders.fields-right')
        </div>

        <div class="col-md-12">
            @include('items.table')
        </div>

        <div class="col-md-6 col-md-offset-6">
            <div class="pull-right">
                <button type="submit" class="btn btn-default">@lang('Save')</button>
            </div>
        </div>
    </form>
@endsection

@section('js')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/uk.js') }}" charset="UTF-8"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    @include('orders.js')
@endsection