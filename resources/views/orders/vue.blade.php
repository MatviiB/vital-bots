@extends('layouts.custom')

@section('content')

    <h2>@lang('Orders')
        <orders-filters
                :statuses="statuses"
                :tracklists="tracklists"
                :dict="dict"
                v-on:query="query"
                v-on:print="print"
        ></orders-filters>
    </h2>

    <orders-index-table
            v-on:alert="alert"
            v-on:set-data="setData"
            :statuses="statuses"
            :tracklists="tracklists"
            :printable="printable"
            :dict="dict"
            ref="orders"
    ></orders-index-table>

    <b-pagination
            v-on:change="paginate"
            ref="pagination"
            :per-page="pagination.perPage"
            :total-rows="pagination.totalRows"
            v-model="pagination.currentPage"
    ></b-pagination>

@endsection