@extends('layouts.custom')

@section('content')

    <h2>Orders
        <div class="pull-right">
            <a href="{{ route('order.create') }}" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span></a>

            <div class="btn-group">
                <a href="{{ route('orders.index', ['filter' => 'newest']) }}" class="btn btn-sm btn-default">Newest</a>
                <a href="{{ route('orders.index', ['filter' => 'week']) }}" class="btn btn-sm btn-warning">One week left</a>
                <a href="{{ route('orders.index', ['filter' => 'overdue']) }}" class="btn btn-sm btn-danger">Overdue</a>
            </div>

            <div class="pull-right" style="margin-left: 5px; margin-top: -3px;">

                <form action="{{ route('orders.index') }}" method="GET" style="display: inline-block;">
                    <select name="status" class="form-control" onchange="this.form.submit()">
                        @if(request('status'))
                            <option value="{{ request('status') }}">{{ str_replace('_', ' ', request('status')) }}</option>
                        @endif
                            <option value="">Status...</option>
                        @foreach(\App\Models\Order::statuses() as $status)
                            <option value="{{ snake_case($status) }}">{{ $status }}</option>
                        @endforeach
                    </select>
                </form>

                <form action="{{ route('orders.index') }}" method="GET" style="display: inline-block">
                    <div class="form-group">
                        <input type="text" class="form-control phone" name="phone" placeholder="Phone" value="{{ request('phone') }}" style="width: 135px;">
                    </div>
                </form>

                <form action="{{ route('orders.index') }}" method="GET" style="display: inline-block">
                    <div class="form-group">
                        <input type="text" class="form-control" name="id" placeholder="ID" value="{{ request('id') }}" style="width: 80px;">
                    </div>
                </form>
            </div>
        </div>
    </h2>

    <table class="table table-hover table-bordered">
        <tr>
            <th class="text-center" style="width: 50px">ID</th>
            <th>Client</th>
            <th>Address</th>
            <th>Items</th>
            <th width="120">Ready/Created</th>
            <th>Note</th>
            <th width="150">Checkout</th>
            <th width="126">Actions</th>
        </tr>
    @foreach($orders as $order)
        <tr>
            <td class="text-center" id="order_id_{{ $order->id }}">
                {{ $order->id }}<br><br>
                <span class="label label-info">{{ $order->user->name }}</span><br><br>
                <span class="label label-default">{{ $order->conversion_status_verbal }}</span>
            </td>
            <td>
                <nobr><a href="{{ route('client.edit', $order->client->id) }}">{{ $order->client->first_name }} {{ $order->client->last_name }}</a></nobr><br>
                @foreach($order->client->phones as $phone)
                <nobr>{{ $phone->phone }}</nobr><br>
                @endforeach
            </td>
            <td>
                {{ $order->address->address }}
                <hr>
                @if($order->delivery) Delivery<br> @endif
                @if(!$order->delivery) Self-Deli<br> @endif
                @if($order->items->sum('assembly')) Assembly<br> @endif
            </td>
            <td>
                <table class="table table-bordered">
                    @foreach($order->items as $item)
                        <tr>
                            <td>
                                {{ $item->name }}
                            </td>
                            <td width="125">
                                <form action="{{ route('item.status.ax', $item->id) }}" method="post" @change="itemStatusFormSubmit">
                                    <select name="status" class="form-control" {{--onchange="this.form.submit()"--}}>
                                        <option value="{{ $item->status }}">{{ $item->status_verbal }}</option>
                                        @foreach(\App\Models\Item::statuses() as $status => $verbal)
                                            @if($status !== $item->status)
                                                <option value="{{ $status }}">{{ $verbal }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </td>
            <td>
                {{ $order->date_ready_verbal }}
                <hr>
                {{ $order->date_created_verbal }}
            </td>
            <td>{{ $order->note }}</td>
            <td>
                <table class="table internal">
                    <tbody>
                        <tr>
                            <td><nobr>Discount: <span class="pull-right">{{ $order->discount ?? '0'}}</span></nobr></td>
                        </tr>
                        <tr @if($order->prepayment && $order->prepayment_status === App\Models\Order::PREPAYMENT_STATUS_NOT_PASSED) style="color: red" @endif>
                            <td><nobr>Prepayment: <span class="pull-right">{{ $order->prepayment ?? '0' }}</span></nobr></td>
                        </tr>
                        <tr>
                            <td><nobr>Delivery: <span class="pull-right">{{ $order->delivery ?? '0' }}</span></nobr></td>
                        </tr>
                        <tr>
                            <td><nobr>Total: <span class="pull-right">{{ $order->total }}</span></nobr></td>
                        </tr>
                        <tr>
                            <td><nobr><strong>Left to Pay: <span class="pull-right">{{ (int)$order->total - (int)$order->prepayment }}</span></strong></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <a href="{{ route('order.edit', $order->id) }}" class="btn btn-sm btn-default" title="Edit">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>

                <a href="{{ route('items.index', ['order_id' => $order->id]) }}" title="Items" class="btn btn-sm btn-default">
                    <span class="glyphicon glyphicon-list"></span>
                </a>

                <form action="{{ route('order.delete', $order->id) }}" method="POST" style="display: inline-block">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" href="{{ route('order.delete', $order->id) }}" class="btn btn-sm btn-default" onclick="if(confirm('Are you sure?')) { return true; } return false;">
                        <span class="glyphicon glyphicon-trash"></span></button>
                </form><br><br>

                <form action="{{ route('order.status', $order->id) }}" method="post">
                    {{ csrf_field() }}
                    <select name="status" class="form-control" onchange="this.form.submit()">
                        <option id="order-{{ $order->id }}-status" value="{{ $order->status }}">{{ $order->status_verbal }}</option>
                        @foreach(\App\Models\Order::statuses() as $status => $verbal)
                            <option value="{{ $status }}" @if($status != \App\Models\Order::STATUS_SHIPPED) disabled @endif>{{ $verbal }}</option>
                        @endforeach
                    </select>
                </form>

                @if($order->prepayment && $order->prepayment_status === App\Models\Order::PREPAYMENT_STATUS_NOT_PASSED)
                    <br><div class="text-center"><a href="{{ route('order.passed', $order->id) }}" class="btn btn-default btn-sm" >PASSED</a></div>
                @endif
            </td>
        </tr>
    @endforeach
    </table>

    {{ $orders->links() }}

@endsection