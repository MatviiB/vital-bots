<div class="form-group">
    <label class="col-sm-4 control-label">@lang('First Name')</label>
    <div class="col-sm-8">
        <input
            type="text" class="form-control" name="client[first_name]"
            @if(isset($client) && !in_array(Route::currentRouteName(), ['client.edit', 'client.create'])) readonly @endif
            @isset($client) value="{{ old('client[first_name]') ?? $client->first_name }}" @endisset
        >
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Last Name')</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="client[last_name]"
               @if(isset($client) && !in_array(Route::currentRouteName(), ['client.edit', 'client.create'])) readonly @endif
               @isset($client) value="{{ old('client[last_name]') ?? $client->last_name }}" @endisset
        >
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Phones')</label>
    <div class="col-sm-8">
        @if(isset($client))
        @foreach($client->phones as $phone)
        <input type="text" class="form-control phone" name="phones[{{ $phone->id }}][phone]" style="margin-top: 5px;"
           @if(isset($client) && !in_array(Route::currentRouteName(), ['client.edit', 'client.create'])) readonly @endif
           @isset($client) value="{{ old('phones['.$phone->id.'][phone]') ?? $phone->phone }}" @endisset
        >
        @endforeach
        @endif
        <input type="text" class="form-control phone" name="phones[new][phone]" style="margin-top: 5px;"
           value="{{ old('phones[new][phone]') ?? null }}" placeholder="Add new number"
        >
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">Email</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="client[email]"
               @if(isset($client) && !in_array(Route::currentRouteName(), ['client.edit', 'client.create'])) readonly @endif
               @isset($client) value="{{ old('client[email]') ?? $client->email }}" @endisset
        >
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Discount')</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="client[discount]"
               @if(isset($client) && !in_array(Route::currentRouteName(), ['client.edit', 'client.create'])) readonly @endif
               @isset($client) value="{{ old('client[discount]') ?? $client->discount }}" @endisset
               @if(!isset($client)) value="0" @endif
        >
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Code')</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" readonly name="code"
               value="{{ $client->code->code ?? \App\Models\Code::genNew() }}">
    </div>
</div>

@if(in_array(Route::currentRouteName(), ['client.edit', 'client.create']))
    <div class="form-group">
        <label class="col-sm-4 control-label">@lang('Provider')</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="client[provider]" readonly
                   value="{{ $client->provider ?? 'web' }}">
        </div>
    </div>
@endif
