@extends('layouts.custom')

@section('content')

    <h2>Edit Client Info
        <a href="{{ route('orders.index', ['client_id' => $client->id]) }}" class="btn btn-default pull-right">
            <span class="glyphicon glyphicon-eye-open" title="Show Clients Orders"></span>
        </a>
    </h2>

    <hr>

    <form class="form-horizontal" action="{{ route('client.update', $client->id) }}" method="POST">

        {{ csrf_field() }}

        <div class="col-md-4">
            @include('clients.fields')
        </div>

        <div class="col-md-4">
            @include('clients.addresses')
        </div>

        <div class="clearfix"></div>

        <div class="col-md-4">
            <div class="text-right">
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>
    </form>
@endsection