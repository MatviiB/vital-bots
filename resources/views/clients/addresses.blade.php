@if(isset($client))
    @if(!in_array(Route::currentRouteName(), ['client.edit', 'client.create']))
        @foreach($client->addresses as $address)
            <div class="form-group">
                <div class="radio">
                    <div class="col-sm-offset-4 col-sm-8">
                        <label>
                            <input type="radio" name="addresses[radio]"
                                @if(!isset($order) && $loop->first)
                                    checked
                                @elseif(isset($order) && $order->address->id == $address->id)
                                    checked
                                @endif
                                value="{{ $address->id }}"
                            >{{ $address->address }}
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">@lang('Floor')</label>
                <div class="col-sm-3">
                    <input type="number" class="form-control" name="addresses[{{ $address->id }}][floor]"
                           value="{{ old('addresses['.$address->id.'][floor]') ?? $address->floor }}">
                </div>

                <div class="col-sm-5">
                    <div class="checkbox">
                        <label class="pull-right">
                            <input type="checkbox" name="addresses[{{ $address->id }}][service_lift]" value="1"
                                   @if(old('addresses['.$address->id.'][service_lift]'))
                                    checked
                                   @elseif($address->service_lift)
                                    checked
                                   @endif
                            >@lang('Service Lift')
                        </label>
                    </div>
                </div>
            </div>
            <hr>

        @endforeach
    @else
        @foreach($client->addresses as $address)
            <div class="form-group">
                <label class="col-sm-4 control-label">@lang('Address')</label>
                <div class="col-sm-8">
                    <textarea class="form-control" rows="1" name="addresses[{{ $address->id }}][address]">{{ old('addresses['.$address->id.'][address]') ?? $address->address }}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">@lang('Floor')</label>
                <div class="col-sm-3">
                    <input type="number" class="form-control" name="addresses[{{ $address->id }}][floor]"
                           value="{{ old('addresses['.$address->id.'][floor]') ?? $address->floor }}">
                </div>
                <div class="col-sm-5">
                    <div class="checkbox">
                        <label class="pull-right">
                            <input type="checkbox" name="addresses[{{ $address->id }}][service_lift]" value="1"
                               @if(old('addresses['.$address->id.'][service_lift]'))
                                checked
                               @elseif($address->service_lift)
                                checked
                               @endif
                            >@lang('Service Lift')
                        </label>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endif

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('New Address')</label>
    <div class="col-sm-8">
        <textarea class="form-control" rows="1" name="addresses[new][address]"
        @if(!isset($client)) required @endif></textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Floor')</label>
    <div class="col-sm-3">
        <input type="number" class="form-control" name="addresses[new][floor]"
               value="{{ old('addresses[new][floor]') ?? null }}">
    </div>
    <div class="col-sm-5">
        <div class="checkbox">
            <label class="pull-right">
                <input type="checkbox" name="addresses[new][service_lift]" value="1"
                    @if(old('addresses[new][service_lift]'))
                        checked
                   @endif
                >@lang('Service Lift')
            </label>
        </div>
    </div>
</div>

