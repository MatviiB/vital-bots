@extends('layouts.custom')

@section('content')

    <h2>@lang('Clients') <a href="{{ route('client.create') }}" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span></a>
        <div class="pull-right">
            <form action="{{ route('clients.index') }}" method="GET" class="pull-right">
                <div class="form-group">
                    <input type="text" class="form-control" name="q" placeholder="Keyword" value="{{ request('q') }}" style="width: 135px;">
                </div>
            </form>

            <form action="{{ route('clients.index') }}" method="GET" class="pull-right">
                <div class="form-group">
                    <input type="text" class="form-control phone" name="phone" placeholder="@lang('Phone')" value="{{ request('phone') }}" style="width: 135px; margin-right: 5px;">
                </div>
            </form>

            <form action="{{ route('clients.index') }}" method="GET" class="pull-right">
                <div class="form-group">
                    <input type="text" class="form-control" name="code" placeholder="@lang('Code')" value="{{ request('code') }}" style="width: 135px; margin-right: 5px;">
                </div>
            </form>
        </div>
    </h2>

    <table class="table table-hover table-bordered">
        <tr>
            <th class="text-center" style="width: 105px">ID / Provider</th>
            <th>@lang('First Name')</th>
            <th>@lang('Phones')</th>
            <th>@lang('Discount')</th>
            <th>@lang('Code')</th>
            <th>Email</th>
            <th style="width: 165px;">@lang('Actions')</th>
        </tr>
        @foreach($clients as $client)
            <tr>
                <td >
                    <div class="pull-left">
                        {{ $client->id }}
                    </div>
                    <div class="pull-right">
                        <span class="label label-info">{{ str_limit($client->provider, 3, '') }}</span>
                    </div>
                </td>
                <td>{{ $client->first_name }} {{ $client->last_name }}</td>
                <td>
                    @foreach($client->phones as $phone)
                        {{ $phone->phone }}<br>
                    @endforeach
                </td>
                <td>{{ $client->discount }}</td>
                <td>{{ $client->code->code ?? null }}</td>
                <td>{{ $client->email }}</td>
                <td>
                    <a href="{{ route('client.edit', $client->id) }}" class="btn btn-sm btn-default">
                        <span class="glyphicon glyphicon-pencil" title="Edit Client Info"></span>
                    </a>

                    <a href="{{ route('order.create', ['client_id' => $client->id]) }}" class="btn btn-sm btn-default">
                        <span class="glyphicon glyphicon-plus" title="Create Order for this Client"></span>
                    </a>

                    <a href="{{ route('orders.index', ['client_id' => $client->id]) }}" class="btn btn-sm btn-default">
                        <span class="glyphicon glyphicon-eye-open" title="Show Clients Orders"></span>
                    </a>

                    <form action="{{ route('client.delete', $client->id) }}" method="POST" style="display: inline-block">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" href="{{ route('client.delete', $client->id) }}" class="btn btn-sm btn-default" onclick="if(confirm('Are you sure?')) { return true; } return false;">
                            <span class="glyphicon glyphicon-trash"></span></button>
                    </form>

                </td>
            </tr>
        @endforeach
    </table>

    {{ $clients->links() }}
@endsection