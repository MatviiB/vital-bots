@extends('layouts.custom')

@section('content')
    <form class="form-horizontal" action="{{ route('client.store') }}" method="POST">

        {{ csrf_field() }}

        <div class="col-md-4">
            @include('clients.fields')
        </div>

        <div class="col-md-4">
            @include('clients.addresses')
        </div>

        <div class="clearfix"></div>

        <div class="col-md-4">
            <div class="text-right">
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>

    </form>
@endsection