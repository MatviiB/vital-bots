<div class="table-responsive ">
    <table class="table table-bordered">
    <thead>
    <tr>
        <th></th>
        <th>ID</th>
        <th>Username</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>
                <input type="checkbox" id="{{ $user->tel_id }}" class="box">
            </td>
            <td>{{ $user->tel_id }}</td>
            <td>{{ $user->username }}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>
                <button class="btn btn-default btn-xs" onclick="deleteUser({{ $user->tel_id }})">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
            </td>
        </tr>
    @endforeach
    </tbody>
    </table>
</div>
{{ $users->links() }}
