@extends('layouts.custom')

@section('content')
    <h2>Users</h2>

    <table class="table table-hover table-bordered">
        <tr>
            <th class="text-center" width="80">ID</th>
            <th>Role</th>
            <th>Name</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        @foreach($users as $user)
        <tr>
            <td class="text-center">{{ $user->id }}</td>
            <td>{{ $user->role }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->role !== 'admin')
                    <a class="btn btn-default btn-sm" href="{{ route('user.login', $user->id) }}">Login as this User</a>
                @endif
            </td>
        </tr>
        @endforeach
    </table>
@endsection