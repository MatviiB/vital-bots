<tr>
    <td>
        <input type='hidden' name='items[{{ $row ?? 'ROW' }}][id]' value='{{ $item->id ?? null }}'>
        <input
                class='form-control'
                type='text'
                name='items[{{ $row ?? 'ROW' }}][name]'
                value='{{ $item->name ?? null }}'
                required
        >
    </td>
    <td>
        <textarea
                class='form-control'
                rows='1'
                name='items[{{ $row ?? 'ROW' }}][description]'
        >{{ $item->description ?? null }}</textarea>
    </td>
    <td>
        <select name='items[{{ $row ?? 'ROW' }}][factory_id]' class='form-control' required>
            <option value=''>None</option>
            @foreach($factories as $factory)
                <option value='{{ $factory->id }}'
                        @if(isset($item) && $item->factory_id === $factory->id)
                        selected
                        @endif>
                    {{ $factory->name }}
                </option>
            @endforeach
        </select>
    </td>
    <td>
        <input
                class='form-control'
                type='text'
                name='items[{{ $row ?? 'ROW' }}][factory_number]'
                value='{{ $item->factory_number ?? null }}'
        >
    </td>
    <td>
        <select name='items[{{ $row ?? 'ROW' }}][status]' class='form-control'>
            @if(isset($item))
                <option value='{{ $item->status }}'>@lang($item->status_verbal)</option>
            @endif
            @foreach(\App\Models\Item::statuses() as $status => $verbal)
                @if(isset($item) and $item->status != $status)
                    <option value='{{ $status }}'>@lang( $verbal )</option>
                @elseif(!isset($item))
                    <option value='{{ $status }}'>@lang( $verbal )</option>
                @endif
            @endforeach
        </select>
    </td>
    <td>
        <input
                class='form-control'
                type='number'
                name='items[{{ $row ?? 'ROW' }}][price_in]'
                value='{{ $item->price_in ?? null }}'
        >
    </td>
    <td>
        <input
                class='form-control price-add'
                type='number'
                name='items[{{ $row ?? 'ROW' }}][price_out]'
                oninput='getTotal(); profit({{ $row ?? 'ROW' }});'
                value='{{ $item->price_out ?? null }}'
                required
        >
    </td>
    <td>
        <input type='text' class='form-control' readonly id='items-{{ $row ?? 'ROW' }}-profit'>
    </td>
    <td>
        <input
                class='form-control price-add'
                type='number'
                name='items[{{ $row ?? 'ROW' }}][lifting]'
                oninput='getTotal()'
                value='{{ $item->lifting ?? null }}'
        >
    </td>
    <td>
        <input
                class='form-control price-add'
                type='number'
                name='items[{{ $row ?? 'ROW' }}][assembly]'
                oninput='getTotal()'
                value='{{ $item->assembly ?? null }}'
        >
    </td>
    <td>
        <button class='btn btn-sm btn-default' id='remove_{{ $row ?? 'ROW' }}' onclick='remove({{ $row ?? 'ROW' }})'>
            <span class='glyphicon glyphicon-trash'></span>
        </button>
    </td>
</tr>
