<table class="table table-hover table-bordered" id="items">
    <tbody>
    <tr>
        <th>@lang('Item Name')</th>
        <th>@lang('Description')</th>
        <th width="200px">@lang('Factory')</th>
        <th>@lang('Factory Number')</th>
        <th width="140">@lang('Status')</th>
        <th width="100">@lang('Price (IN)')</th>
        <th width="100">@lang('Price (OUT)')</th>
        <th class="text-center" width="60">%</th>
        <th width="100">@lang('Lifting')</th>
        <th width="100">@lang('Assembly')</th>
        <th width="50">
            <button class="btn btn-sm btn-default" onclick="plus()">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
        </th>
    </tr>

    @if(isset($order->items))
        @foreach($order->items as $row => $item)
            @include('items.row', [
                'row' => $row + 1,
                'item' => $item
            ])
        @endforeach
    @else
        @include('items.row', ['row' => 1])
    @endif
    </tbody>
</table>