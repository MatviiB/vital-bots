@extends('layouts.custom')

@section('content')
    <h2>@lang('Items')
        <div class="pull-right">
            <form action="{{ route('items.index') }}" method="GET" style="display: inline-block;">
                @foreach(request()->except('status') as $name => $value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <select name="status" class="form-control" onchange="this.form.submit()">
                    @if(request('status'))
                        <option value="{{ request('status') }}">{{ str_replace('_', ' ', request('status')) }}</option>
                    @endif
                    <option value="">Status...</option>
                    @foreach(\App\Models\Item::statuses() as $status)
                        <option value="{{ snake_case($status) }}">{{ $status }}</option>
                    @endforeach
                </select>
            </form>

            <form action="{{ route('items.index') }}" method="GET" style="display: inline-block;">
                @foreach(request()->except('factory_id') as $name => $value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <select name="factory_id" class="form-control" onchange="this.form.submit()">
                    @if(request('factory_id'))
                        <option value="{{ request('factory_id') }}">
                            {{ $factories->first(function($factory) {
                                    return $factory->id == request('factory_id');
                               })->name
                           }}
                           </option>
                    @endif
                    <option value="">Factory...</option>
                    @foreach($factories as $factory)
                        <option value="{{ $factory->id }}">{{ $factory->name}}</option>
                    @endforeach
                </select>
            </form>
        </div>
    </h2>
        <table class="table table-hover table-bordered">
            <tr>
                <th class="text-center" style="width: 50px">IDs</th>
                <th>Name</th>
                <th>Description</th>
                <th>Factory</th>
                <th>Factory Number</th>
                <th width="100px">Price (IN)</th>
                <th width="100px">Price (OUT)</th>
                <th width="100px">Lifting</th>
                <th width="100px">Assembly</th>
                <th width="125" class="text-center">Status</th>
                {{--<th style="width: 90px;">Actions</th>--}}
            </tr>

            @foreach($items as $item)
                <tr>
                    <td class="text-center">
                        <a href="{{ route('orders.index', ['id' => $item->order_id] ) }}" title="Go to Order">{{ $item->order_id }}</a>-{{ $item->id }}
                    </td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $factories->first(function($factory) use ($item) {
                                return $factory->id == $item->factory_id;
                            })->name }}</td>
                    <td>{{ $item->factory_number }}</td>
                    <td>{{ $item->price_in }}</td>
                    <td>{{ $item->price_out }}</td>
                    <td>{{ $item->lifting }}</td>
                    <td>{{ $item->assembly }}</td>
                    <td class="text-center">
                        <form action="{{ route('item.status', $item->id) }}" method="post">
                            {{ csrf_field() }}
                            <select class="form-control" name="status" onchange="this.form.submit()">
                                <option value="{{ $item->status }}">{{ $item->status_verbal }}</option>
                                @foreach(\App\Models\Item::statuses() as $status => $verbal)
                                    @if($status !== $item->status)
                                        <option value="{{ $status }}">{{ $verbal }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

    {{ $items->links() }}
@endsection