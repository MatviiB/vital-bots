@extends('layouts.custom')

@section('content')
    <h2>Items
        <div class="pull-right">
            <form action="{{ route('items.index') }}" method="GET" style="display: inline-block;">
                @foreach(request()->except('status') as $name => $value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <select name="status" class="form-control" onchange="this.form.submit()">
                    @if(request('status'))
                        <option value="{{ request('status') }}">@lang(str_replace('_', ' ', request('status')))</option>
                    @endif
                    <option value="">@lang('Status...')</option>
                    @foreach(\App\Models\Item::statuses() as $status)
                        <option value="{{ snake_case($status) }}">@lang("$status")</option>
                    @endforeach
                </select>
            </form>

            <form action="{{ route('items.index') }}" method="GET" style="display: inline-block;">
                @foreach(request()->except('factory_id') as $name => $value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach

                <select name="factory_id" class="form-control" onchange="this.form.submit()">
                    @if(request('factory_id'))
                        <option value="{{ request('factory_id') }}">
                            {{ $factories->first(function($factory) {
                                    return $factory->id == request('factory_id');
                               })->name
                           }}
                        </option>
                    @endif
                    <option value="">@lang('Factory')...</option>
                    @foreach($factories as $factory)
                        <option value="{{ $factory->id }}">{{ $factory->name}}</option>
                    @endforeach
                </select>
            </form>
        </div>
    </h2>

    <items-table
            v-on:alert="alert"
            v-on:set-data="setData"
            :statuses="statuses"
            :dict="dict"
            ref="items"
    ></items-table>

    <b-pagination
            v-on:change="paginate"
            :per-page="pagination.perPage"
            :total-rows="pagination.totalRows"
            v-model="pagination.currentPage"
            ref="pagination"
    ></b-pagination>
@endsection