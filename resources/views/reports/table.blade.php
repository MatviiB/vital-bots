<table class="table table-hover table-bordered">
    @unless(count($data))
        <tr>@lang('No Results')</tr>
    @else
    <tr>
        @foreach(array_keys($data[0]) as $field)
        <th>@lang($field)</th>
        @endforeach
    </tr>
    @endunless

    @foreach($data as $item)
        <tr>
            @foreach($item as $field)
                <td>
                    @lang($field)
                </td>
            @endforeach
        </tr>
    @endforeach
</table>