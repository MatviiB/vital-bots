<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/uk.js') }}" charset="UTF-8"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script>
    $.each(document.getElementsByClassName("datepicker"), function() {
        $(this).datetimepicker({
            format: 'YYYY-MM-DD',
            locale: 'uk',
        });
    });
</script>