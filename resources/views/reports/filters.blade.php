<div class="pull-right">
    <form action="{{ url() }}" method="GET" style="display: inline-block;" class="form-inline">
        <div class='input-group date datepicker'>
            <input type='text' class="form-control" name="date_from"
                   value="{{ request('date_from') }}">
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>

        <div class='input-group date datepicker'>
            <input type='text' class="form-control" name="date_to"
                   value="{{ request('date_to') }}">
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>

        @section('other-filters')

        <button class="btn btn-primary btn-sm" type="submit">@lang('Filter')</button>
    </form>

    <a href="{{ request()->fullUrlWithQuery(["export" => true]) }}" class="btn btn-default btn-sm">@lang('Export')</a>
</div>