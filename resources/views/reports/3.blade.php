@extends('layouts.custom')

@section('content')
    <h2>@lang('Report 3')
        <div class="pull-right">
            <form action="{{ route('reports.3') }}" method="GET" style="display: inline-block;" class="form-inline">
                <div class='input-group date datepicker'>
                    <input type='text' class="form-control" name="date_from"
                           value="{{ request('date_from') }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>

                <div class='input-group date datepicker'>
                    <input type='text' class="form-control" name="date_to"
                           value="{{ request('date_to') }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>

                <select name="factory_id" class="form-control">
                    @if(request('user_id'))
                        <option value="{{ request('factory_id') }}">
                            {{ $factories->first(function($factory) {
                                    return $factory->id == request('factory_id');
                               })->name
                           }}
                        </option>
                    @endif
                    <option value="">@lang('Factory...')</option>
                    @foreach($factories as $factory)
                        <option value="{{ $factory->id }}">{{ $factory->name}}</option>
                    @endforeach
                </select>

                <button class="btn btn-primary btn-sm" type="submit">@lang('Filter')</button>
            </form>

            <a href="{{ request()->fullUrlWithQuery(["export" => true]) }}" class="btn btn-default btn-sm">@lang('Export')</a>
        </div>
    </h2>

    @include('reports.table')

@endsection

@section('js')
    @include('reports.datepicker')
@endsection