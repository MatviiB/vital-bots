@extends('layouts.custom')

@section('content')
    <form class="form-horizontal" action="{{ route('texts.update') }}" method="POST">
        {{ csrf_field() }}
        @foreach($texts as $text)
        <div class="form-group">
            <label class="col-sm-2 control-label">{{ $text->key }}</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="{{ $text->key }}">{{ $text->value }}</textarea>
            </div>
        </div>
        @endforeach

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Save</button>
            </div>
        </div>
    </form>
@endsection