@extends('layouts.custom')

@section('content')

    <h2>@lang('Factories')</h2>

    <div class="col-md-6 col-md-offset-3">
        <table class="table table-hover table-bordered">
            <tr>
                <th class="text-center" style="width: 50px">ID</th>
                <th>@lang('Name')</th>
                <th>@lang('Address')</th>
                <th style="width: 90px;">@lang('Actions')</th>
            </tr>

            @foreach($factories as $factory)
                <tr>
                    <form action="{{ route('factory.update', $factory->id) }}" method="POST" style="display: inline-block">
                        {{ csrf_field() }}
                    <td class="text-center">
                        {{ $factory->id }}
                    </td>
                    <td>
                        <input class="form-control" type="text" name="name" value="{{ $factory->name }}">
                    </td>
                    <td>
                        <input class="form-control" type="text" name="address" value="{{ $factory->address }}">
                    </td>
                    <td>

                            <button type="submit" href="{{ route('factory.update', $factory->id) }}" class="btn btn-sm btn-default">
                                <span class="glyphicon glyphicon-floppy-disk"></span>
                            </button>
                        </form>

                        <form action="{{ route('factory.delete', $factory->id) }}" method="POST" style="display: inline-block">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" href="{{ route('factory.delete', $factory->id) }}" class="btn btn-sm btn-default" onclick="if(confirm('Are you sure?')) { return true; } return false;">
                                <span class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </td>
                </tr>
            @endforeach

            <tr>
                <form action="{{ route('factory.store') }}" method="POST" style="display: inline-block">
                    {{ csrf_field() }}
                    <td>
                        <span class="badge">new</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="name">
                    </td>
                    <td>
                        <input class="form-control" type="text" name="address">
                    </td>
                    <td>
                        <button type="submit" href="{{ route('factory.store') }}" class="btn btn-sm btn-default">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                        </button>
                    </td>
                </form>
            </tr>
        </table>
    </div>

@endsection