@extends('layouts.custom')

@section('content')
    <form class="form-horizontal" action="{{ route('tracklist.store') }}" method="POST">

        {{ csrf_field() }}

        <div class="col-md-4">
            @include('tracking.fields')
        </div>

        <div class="clearfix"></div>

        <div class="col-md-4">
            <div class="text-right">
                <button type="submit" class="btn btn-default">@lang('Save')</button>
            </div>
        </div>

    </form>
@endsection

@section('js')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/uk.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $.each(document.getElementsByClassName("datepicker"), function() {
            $(this).datetimepicker({
                format: 'DD/MM/YYYY',
                locale: 'uk'
            });
        });
    </script>
@endsection