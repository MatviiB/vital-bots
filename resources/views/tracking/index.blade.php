@extends('layouts.custom')

@section('content')

    <h2>@lang('Tracking') <a href="{{ route('tracklist.create') }}" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span></a>
    </h2>

    <div class="col-md-6">
        <table class="table table-hover table-bordered">
            <tr>
                <th class="text-center" style="width: 60px">ID</th>
                <th>@lang('Name')</th>
                <th>@lang('Date')</th>
                <th style="width: 135px;">@lang('Actions')</th>
            </tr>
            @foreach($tracklists as $tracklist)
                <tr>
                    <td class="text-center">{{ $tracklist->id }}</td>
                    <td>{{ $tracklist->name }}</td>
                    <td>{{ $tracklist->date_track }}</td>
                    <td>
                        <a href="{{ route('tracklist.edit', $tracklist->id) }}" class="btn btn-sm btn-default">
                            <span class="glyphicon glyphicon-pencil" title="Edit Tracklist Info"></span>
                        </a>

                        <a href="{{ route('orders.index', ['tracklist' => $tracklist->id]) }}" class="btn btn-sm btn-default">
                            <span class="glyphicon glyphicon-list" title="Tracklist`s Orders"></span>
                        </a>

                        <form action="{{ route('tracklist.delete', $tracklist->id) }}" method="POST" style="display: inline-block">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit" href="{{ route('tracklist.delete', $tracklist->id) }}" class="btn btn-sm btn-default" onclick="if(confirm('Are you sure?')) { return true; } return false;">
                                <span class="glyphicon glyphicon-trash"></span></button>
                        </form>

                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    {{ $tracklists->links() }}
@endsection