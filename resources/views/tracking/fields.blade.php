<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Name')</label>
    <div class="col-sm-8">
        <input
            type="text" class="form-control" name="name"
            @isset($tracklist) value="{{ old('tracklist[name]') ?? $tracklist->name }}" @endisset
        >
    </div>
</div>

<div class="form-group">
    <label class="col-sm-4 control-label">@lang('Date')</label>
    <div class="col-sm-8">
        <div class='input-group date datepicker'>
            <input type='text' class="form-control" name="date" required
                   @if(isset($tracklist)) value="{{ \Carbon\Carbon::parse($tracklist->date)->format('d/m/Y') }}" @else value="" @endif>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
        </div>
    </div>
</div>


