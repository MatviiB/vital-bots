<?php

namespace App\Http\Controllers;

use App\Models\Factory;
use App\Models\Item;

use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function index()
    {
        $factories = Factory::all();

        return view('items.vue', compact('factories'));
    }

    public function axIndex(Request $request)
    {
        $items = Item::index($request);

        if ($request->filled('data')) {
            $factories = Factory::all();
            $statuses = Item::statuses();

            return response(compact('factories', 'statuses', 'items'));
        }

        return response(compact('items'));
    }

    public function status($id, Request $request)
    {
        $item = Item::find($id);
        $item->update(['status' => $request->get('status')]);

        $item->order->updateStatus();

        return redirect()->back()
            ->with('message', 'Status Updated.');
    }

    public function axStatus($id, Request $request)
    {
        $item = Item::find($id);
        $item->update(['status' => $request->get('status')]);

        $item->order->updateStatus();

        return response([
            'order_status'        => $item->order->status,
            'order_status_verbal' => $item->order->status_verbal,
            'order_tracklist'     => $item->order->tracklist,
            'message'             => 'Status Updated.'
        ], 200);
    }

    public function axUpdate($id, Request $request)
    {
        $item = Item::find($id);

        $item->update($request->all());

        return response([
            'message'             => 'Saved.'
        ], 200);
    }
}
