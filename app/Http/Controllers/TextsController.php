<?php

namespace App\Http\Controllers;

use App\Models\Text;

use Illuminate\Http\Request;

class TextsController extends Controller
{
    public function texts()
    {
        $texts = Text::all();

        return view('texts', compact('texts'));
    }

    public function update(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            Text::where('key', $key)->update(['value' => $value]);
        }

        return redirect('texts');
    }
}
