<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Models\Order;
use App\Models\Tracklist;
use Illuminate\Http\Request;

class OrdersAxController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::index($request);

        if ($request->filled('data')) {
            $tracklists = Tracklist::whereDate('date', '>=', Carbon::now()->startOfDay())->get();
            $statuses = Order::statuses();

            return response(compact('tracklists', 'statuses', 'orders'));
        }

        return response(compact('orders'));
    }

    public function delete($id)
    {
        $order = Order::find($id);

        if ($order->discount) {
            $order->client->discount += $order->discount;
            $order->client->save();
        }
        $order->items()->delete();
        $order->delete();

        return response(['message' => 'Order was successfully deleted'], 200);
    }

    public function passed($id)
    {
        $order = Order::find($id);
        $order->prepayment_status = Order::PREPAYMENT_STATUS_PASSED;
        $order->save();

        return response(['message' => 'Prepayment was set as "passed"'], 200);
    }

    public function tracklist(Request $request)
    {
        $order = Order::find($request->get('order'));
        $order->tracklist_id = $request->get('tracklist');
        $order->save();

        return response(['message' => 'Order successfully added to tracklist.'], 200);
    }

    public function tracklistOrder(Request $request)
    {
        $order = Order::find($request->get('order_id'));
        $order->tracking_order = $request->get('tracking-order');
        $order->save();

        return response([
//            'order' => $order,
            'message' => 'Tracking position changed.'], 200);
    }
}