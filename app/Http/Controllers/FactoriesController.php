<?php

namespace App\Http\Controllers;

use App\Models\Factory;

use Illuminate\Http\Request;

class FactoriesController extends Controller
{
    public function index()
    {
        $factories = Factory::paginate();

        return view('factories.index', compact('factories'));
    }

    public function store(Request $request)
    {
        Factory::create($request->all());

        return redirect()->route('factories.index');
    }

    public function update(Request $request, $id)
    {
        Factory::find($id)->update($request->all());

        return redirect()->route('factories.index');
    }

    public function delete($id)
    {
        Factory::destroy([$id]);

        return redirect()->route('factories.index');
    }
}
