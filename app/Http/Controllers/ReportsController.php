<?php

namespace App\Http\Controllers;

use App\Exports\ExportReportOne;
use App\Exports\ExportReportTwo;
use App\Exports\ExportReportThree;
use App\Models\Factory;
use App\Models\Item;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    public function report_1(Request $request)
    {
        if ($request->get('export')) {
            return Excel::download(new ExportReportOne(),
                Lang::getFromJson('Report 1') . '.xlsx'
            );
        }

        $data = Item::report_1($request);
        $users = User::all();

        return view('reports.1', compact('data', 'users'));
    }

    public function report_2(Request $request)
    {
        if ($request->get('export')) {
            return Excel::download(new ExportReportTwo(),
                Lang::getFromJson('Report 2') . '.xlsx'
            );
        }

        $data = Order::report_2($request);

        return view('reports.2', compact('data'));
    }

    public function report_3(Request $request)
    {
        if ($request->get('export')) {
            return Excel::download(new ExportReportThree(),
                Lang::getFromJson('Report 3') . '.xlsx'
            );
        }

        $data = Item::report_3($request);
        $factories = Factory::all();

        return view('reports.3', compact('data', 'factories'));
    }
}
