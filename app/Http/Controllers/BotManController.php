<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Text;
use App\Models\Client;

use Milon\Barcode\DNS1D;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;

class BotManController extends Controller
{

    protected $texts;

    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');
        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    public function start(BotMan $bot)
    {
        $bot->reply(Text::where('key', 'subscribed')->first()->value);
    }

    public function code(BotMan $bot)
    {
        $user = $bot->getUser();

        $client = Client::where('provider_id', $user->getId())->first();

        if (!$client) {
            $client = Client::create([
                    'provider_id' => $user->getId(),
                    'username' => $user->getUsername(),
                    'first_name' => $user->getFirstName(),
                    'last_name' => $user->getLastName(),
                    'provider' => 'telegram'
                ]);

            $code = Code::create([
                    'user_id' => $client->id,
                    'code' => Code::genNew(),
                ]);
        } else {
            $code = Code::where('user_id', $client->id)->first();
        }

        $attachment = new Image(env('APP_URL') . DNS1D::getBarcodePNGPath(str_replace(' ', '', $code->code), "EAN5", 5, 50));

        $message = OutgoingMessage::create(str_replace(
            '{{ code }}',
            $code->code,
            Text::where('key', 'code')
                ->first()->value))
            ->withAttachment($attachment);

        $bot->reply($message);
    }

    public function balance(BotMan $bot)
    {
        $id = $user = $bot->getUser()->getId();

        $client = Client::where('provider_id', $id)->first();

        if (!$client) {
            return false;
        }

        $bot->reply(str_replace(
                    '{{ discount }}',
                    $client->discount,
                    Text::where('key', 'your_discount')
                        ->first()->value
                ));
    }

    public function info(BotMan $bot)
    {
        $bot->reply(Text::where('key', 'info')
            ->first()->value);
    }
}
