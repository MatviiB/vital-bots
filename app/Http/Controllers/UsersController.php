<?php

namespace App\Http\Controllers;

use App\Models\User;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::get();

        return view('users.index', compact('users'));
    }

    public function login($id)
    {
        \Auth::loginUsingId($id);

        return redirect('/');
    }
}
