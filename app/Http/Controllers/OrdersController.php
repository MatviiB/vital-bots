<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Item;
use App\Models\Order;
use App\Models\Phone;
use App\Models\Client;
use App\Models\Address;
use App\Models\Factory;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::index($request);

        return view('orders.index', compact('orders'));
    }

    public function edit($id)
    {
        $factories = Factory::get();
        $order = Order::with('items')->find($id);
        $client = $order->client;

        return view('orders.edit',
            compact('order', 'client', 'factories'));
    }

    public function update($id, Request $request)
    {
//        DB::enableQueryLog();

        $request = $request->all();

        $order = Order::with('client', 'items')->find($id);

        DB::beginTransaction();

        try {
            $order->client->update($request['client']);

            Phone::updatePhones($request['phones'], $order->client->id);

            if ($request['addresses']['new']['address']) {
                $address = Address::create(
                    $request['addresses']['new'] + ['client_id' => $order->client->id]
                );
            } else {
                Address::where('id', $request['addresses']['radio'])
                    ->update([
                        'floor' => $request['addresses'][$request['addresses']['radio']]['floor'] ?? 0,
                        'service_lift' => $request['addresses'][$request['addresses']['radio']]['service_lift'] ?? 0
                    ]);
            }

            $request['order']['address_id'] = $address->id ?? $request['addresses']['radio'];

            if ($request['order']['date_created']) {
                $request['order']['date_created'] = Carbon::createFromFormat(
                    'd/m/Y', $request['order']['date_created']
                )->toDateTimeString();
            } else {
                $request['order']['date_created'] = Carbon::now()->toDateTimeString();
            }

            if ($request['order']['date_ready']) {
                $request['order']['date_ready'] = Carbon::createFromFormat(
                    'd/m/Y', $request['order']['date_ready']
                )->toDateTimeString();
            } else {
                $request['order']['date_ready'] = Carbon::now()->addDays(3)->toDateTimeString();
            }

            $request['order']['delivery'] = $request['order']['delivery'] ?? 0;

            $order->update(array_filter($request['order']));

            if (!isset($request['order']['prepayment_status'])) {
                $order->prepayment_status = Order::PREPAYMENT_STATUS_NOT_PASSED;
                $order->save();
            }

            $items_ids = $order->items->pluck('id')->toArray();

            foreach ($request['items'] as $item) {
                if ($item['id']) {
                    $order->items()->where('id', $item['id'])->update(array_filter($item));
                    unset($items_ids[array_search($item['id'], $items_ids)]);
                } else {
                    $order->items()->create(array_filter($item));
                }
            }

            $order->items()->whereIn('id', $items_ids)->delete();

            $order->updateStatus();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd([
                'm' => $e->getMessage(),
                'f' => $e->getFile(),
                'l' => $e->getLine()
            ]);
        }

//        dd(DB::getQueryLog());

        return redirect()
            ->route('order.edit', $order->id)
            ->with('message', 'Updated successfully!');
    }

    public function create(Request $request)
    {
        $factories = Factory::get();
        $html_row = str_replace(["\r","\n"], "", \View::make('items.row', compact('factories')));

        if ($request->has('client_id')) {
            $client = Client::with('phones', 'addresses')
                ->find($request->get('client_id'));

            return view('orders.create', compact('client', 'factories', 'html_row'));
        }

        return view('orders.create', compact('factories'));
    }

    public function store(Request $request)
    {
        $request = $request->all();

        DB::beginTransaction();

        try {
            if (!$request['order']['client_id']) {
                $client = Client::create(array_filter($request['client']));

                Code::create([
                    'user_id' => $client->id,
                    'code' => $request['code']
                ]);

                $request['order']['client_id'] = $client->id;
            } else {
                $client = Client::find($request['order']['client_id']);
                $client->update($request['client']);
            }

            Phone::updatePhones($request['phones'], $client->id);

            if ($request['addresses']['new']['address']) {
                $address = Address::create(
                    $request['addresses']['new'] + ['client_id' => $request['order']['client_id']]
                );
            }

            $request['order']['address_id'] = $address->id ?? $request['addresses']['radio'];

            if ($request['order']['date_created']) {
                $request['order']['date_created'] = Carbon::createFromFormat(
                    'd/m/Y', $request['order']['date_created']
                )->toDateTimeString();
            } else {
                $request['order']['date_created'] = Carbon::now()->toDateTimeString();
            }

            if ($request['order']['date_ready']) {
                $request['order']['date_ready'] = Carbon::createFromFormat(
                    'd/m/Y', $request['order']['date_ready']
                )->toDateTimeString();
            } else {
                $request['order']['date_ready'] = Carbon::now()->addDays(3)->toDateTimeString();
            }

            $order = Order::create(array_filter($request['order']));

            foreach ($request['items'] as $item) {
                $item['order_id'] = $order->id;
                Item::create(array_filter($item));
            }

            $order->updateStatus();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd([
                'm' => $e->getMessage(),
                'f' => $e->getFile(),
                'l' => $e->getLine()
            ]);
        }

        return redirect()
            ->route('order.edit', $order->id)
            ->with('message', 'Order created successfully!');
    }

    public function delete($id)
    {
        $order = Order::find($id);

        if ($order->discount) {
            $order->client->discount += $order->discount;
            $order->client->save();
        }
        $order->items()->delete();
        $order->delete();

        return redirect()->back()->with('message', 'Order Deleted Successfully.');
    }

    public function passed($id)
    {
        $order = Order::find($id);
        $order->prepayment_status = Order::PREPAYMENT_STATUS_PASSED;
        $order->save();

        return redirect()->back()->with('message', 'Prepayment Status Updated.');
    }

    /*public function status($id, Request $request)
    {
        if ($request->get('status') == Order::STATUS_SHIPPED) {

            $order = Order::with('items')->find($id);

            if ($order->status == Order::STATUS_SHIPPED) {
                return redirect()->back();
            }

            foreach ($order->items as $item) {
                $item->status = Item::STATUS_SHIPPED;
                $item->save();
            }

            $order->status = Order::STATUS_SHIPPED;
            $order->save();

            $order->sendBonus();

            return redirect()->back()->with('message', 'Status Updated.');
        }

        return redirect()->back();
    }*/
}
