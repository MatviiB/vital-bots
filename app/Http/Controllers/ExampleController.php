<?php

namespace App\Http\Controllers;

use App\TelegramUser;

use Illuminate\Http\Request;

class ExampleController extends Controller
{
    /**
     * Get users for table, with pagination
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsers(Request $request)
    {
        $users = TelegramUser::paginate(1);
        return view('table', compact('users'));
    }

    /**
     * Unsubscribe users from channel
     *
     * @param Request $request
     */
    public function postUsersUnsubscribe(Request $request)
    {
        TelegramUser::whereIn('tel_id', $request->get('users'))->delete();
    }

    /**
     * Delete user from channel
     *
     * @param $tel_id
     */
    public function deleteUser($tel_id)
    {
        TelegramUser::where('tel_id', $tel_id)->delete();
    }

    /**
     * Return view built with Vue
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getVue()
    {
        return view('vue');
    }

    /**
     * Get list of users for Vue list with pagination parameters
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersVue(Request $request)
    {
        $items = TelegramUser::latest()->paginate(1);

        $response = [
            'pagination' => [
                'total' => $items->total(),
                'per_page' => $items->perPage(),
                'current_page' => $items->currentPage(),
                'last_page' => $items->lastPage(),
                'from' => $items->firstItem(),
                'to' => $items->lastItem()
            ],
            'data' => $items
        ];

        return response()->json($response);
    }
}
