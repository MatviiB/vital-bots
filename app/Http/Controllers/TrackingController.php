<?php

namespace App\Http\Controllers;

use App\Models\Tracklist;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrackingController extends Controller
{
    public function index()
    {
        $tracklists = Tracklist::latest()->paginate();

        return view('tracking.index', compact('tracklists'));
    }

    public function create()
    {
        return view('tracking.create');
    }

    public function store(Request $request)
    {

        $id = Tracklist::create([
            'name' => $request->get('name'),
            'date' => Carbon::createFromFormat('d/m/Y', $request->get('date'))
        ]);

        return redirect()->route('tracklist.edit', $id)
            ->with('message', 'Tracklist created Successfully.');
    }

    public function edit($id)
    {
        $tracklist = Tracklist::find($id);

        return view('tracking.edit', compact('tracklist'));
    }

    public function update(Request $request, $id)
    {
        $tracklist = Tracklist::find($id);

        $tracklist->name = $request->get('name');
        $tracklist->date = Carbon::createFromFormat('d/m/Y', $request->get('date'));
        $tracklist->save();

        return redirect()->back()
            ->with('message', 'Tracklist updated Successfully.');
    }

    public function delete($id)
    {
        Tracklist::delete($id);

        return redirect()->route('tracklists.index');
    }
}
