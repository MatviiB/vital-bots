<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Code;
use App\Models\Text;
use App\Models\Client as VMClient;

use Viber\Bot;
use Viber\Client;
use Viber\Api\Sender;
use Milon\Barcode\DNS1D;

use Illuminate\Http\Request;


class ViberController extends Controller
{
    protected $texts;

    public function webhook(Request $request)
    {
        $botSender = new Sender([
            'name' => 'Bot',
            'avatar' => 'https://bots.vitalmebli.com.ua/images/bot.jpg',
        ]);

        $this->texts = Text::all();

        try {
            $bot = new Bot(['token' => config('botman.viber.token')]);

            $bot
                ->onSubscribe(function ($event) use ($bot, $botSender, $request) {

                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($event->getUser()->getId())
                            ->setText($this->texts->where('key', 'subscribed')->first()->value));
                })

                ->onText('|ping|', function ($event) use ($bot, $botSender) {
                    // .* - match any symbols (see PCRE)
                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($event->getSender()->getId())
                            ->setText("pong"));
                })

                ->onText('|код|', function ($event) use ($bot, $botSender) {

                    $id = $event->getSender()->getId();

                    $client = VMClient::where('provider_id', $id)->first();

                    if (!$client) {
                        $client = VMClient::create([
                            'provider_id' => $id,
                            'username' => $event->getSender()->getName(),
                            'first_name' => explode(' ', $event->getSender()->getName())[0],
                            'last_name' => explode(' ', $event->getSender()->getName())[1] ?? null,
                            'avatar' => $event->getSender()->getAvatar() ?? null,
                            'provider' => 'viber'
                        ]);

                        $code = Code::create([
                            'user_id' => $client->id,
                            'code' => Code::genNew()
                        ]);
                    } else {
                        $code = Code::where('user_id', $client->id)->first();
                    }

                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($id)
                            ->setText(str_replace(
                                '{{ code }}',
                                $code->code,
                                $this->texts
                                    ->where('key', 'code')
                                    ->first()->value
                            )));

                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Picture())
                            ->setSender($botSender)
                            ->setReceiver($id)
                            ->setMedia(env('APP_URL') . DNS1D::getBarcodePNGPath(str_replace(' ', '', $code->code), "EAN5", 5, 50))
                    );
                })

                ->onText('|баланс|', function ($event) use ($bot, $botSender) {

                    $id = $event->getSender()->getId();

                    $client = VMClient::where('provider_id', $id)->first();

                    if (!$client) {
                        return false;
                    }

                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($id)
                            ->setText(str_replace(
                                '{{ discount }}',
                                $client->discount,
                                $this->texts
                                    ->where('key', 'your_discount')
                                    ->first()->value
                            )));
                })

                ->onText('|інфо|', function ($event) use ($bot, $botSender) {

                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($event->getSender()->getId())
                            ->setText(
                                $this->texts
                                    ->where('key', 'info')
                                    ->first()->value
                            ));
                })

                ->run();

            } catch (\Exception $e) {
                Log::debug($e);
            }


            /*if ($request->get('event') === 'message' &&
                $request->get('message')['type'] === 'text' &&
                $request->get('message')['text'] === 'ping') {*/
            $url = 'https://im.bitrix.info/imwebhook/eh/050bd7328e1be26144ac95994857ba9a1505927215/';
            $headers = [
                'Content-Type: application/json',
            ];
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->all()));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($ch);
            curl_close($ch);
//        }
    }

    public function register()
    {
        try {
            $client = new Client([ 'token' =>  config('botman.viber.token') ]);
            $result = $client->setWebhook('https://bots.vitalmebli.com.ua/viber/webhook');
            echo "Success!\n";
        } catch (\Exception $e) {
            echo "Error: ". $e->getMessage() ."\n";
        }
    }
}
