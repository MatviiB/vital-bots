<?php

namespace App\Http\Controllers;

use App\Models\Code;
use App\Models\Client;
use App\Models\Address;

use App\Models\Phone;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index(Request $request)
    {
        $clients = Client::with('code', 'phones');
        if ($request->filled('q')) {
            $clients->where('first_name', 'LIKE', '%' . $request->get('q') . '%')
                ->orWhere('last_name', 'LIKE', '%' . $request->get('q') . '%');
        } elseif ($request->filled('phone')) {
            $clients->whereHas('phones', function ($q) use ($request) {
                $q->where('phone', preg_replace('/\D/', '', $request->get('phone')));
            });
        } elseif ($request->filled('code')) {
            $a = mb_substr($request->get('code'), 0, 3);
            $b = mb_substr($request->get('code'), -4);
            $code = implode(' ', [$a, $b]);

            $clients->orWhereHas('code', function ($query) use ($code) {
                $query->where('code', $code);
            });
        } else {
            $clients->latest();
        }

        $clients = $clients->paginate();

        return view('clients.index', compact('clients'));
    }

    public function edit($id)
    {
        $client = Client::with('phones', 'addresses')->find($id);

        return view('clients.edit', compact('client'));
    }

    public function update(Request $request, $id)
    {
        Client::where('id', $id)->update($request->get('client'));
        Phone::updatePhones($request->get('phones'), $id);
        Address::updateAddresses($request->get('addresses'), $id);

        return redirect()->back()->with('message', 'Client info successfully Updated.');
    }

    public function create()
    {
        $code = Code::genNew();

        return view('clients.create', compact('code'));
    }

    public function store(Request $request)
    {
        $request = $request->all();

        $client = Client::create($request['client']);
        Phone::updatePhones($request['phones'], $client->id);

        Code::create([
            'user_id' => $client->id,
            'code' => $request['code']
        ]);

        if (!empty($request['addresses']['new']['address'])) {
            Address::create($request['addresses']['new'] + ['client_id' => $client->id]);
        }

        return redirect()->route('client.edit', $client->id)
            ->with('message', 'Client info successfully Created.');
    }

    public function delete($id)
    {
        $client = Client::find($id);
        foreach ($client->orders() as $order) {
            $order->items()->delete();
        }
        $client->orders()->delete();
        $client->addresses()->delete();
        Code::where('user_id', $client->id)->delete();
        $client->delete();

        return redirect()->route('clients.index')
            ->with('message', 'Client deleted successfully.');
    }
}
