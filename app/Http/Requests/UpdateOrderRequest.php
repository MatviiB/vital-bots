<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        dd($this->request()->all());
        $request = $this->request->all();

        $rules = [
            'client_id' => 'required',
            'user_id' => 'required',
            'total' => 'required',
            'date_ready' => 'required'
        ];

        if ($this->request->has('address-new')) {
            $rules['address-new'] = 'required';
        }

        if ($this->request->has('address_id')) {
            $rules['address_id'] = 'required';
        }

        if (empty($request['items']['new'])) {
            $rules['order_items'] = 'required';

            return $rules;
        }

        foreach ($request['items']['new'] as $item) {

            foreach ($item as $field => $value) {
                if (empty($value)) {
                    $rules["order_items_$field"] = 'required';
                }
            }

        }

        return $rules;
    }
}
