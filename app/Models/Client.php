<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'provider_id',
        'username',
        'first_name',
        'last_name',
        'phone',
        'email',
        'avatar',
        'subscribed',
        'provider',
        'discount',
    ];

    public function phones()
    {
        return $this->hasMany('App\Models\Phone');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function code()
    {
        return $this->hasOne('App\Models\Code', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\Address');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function getBonusPercentageAttribute()
    {
        $sum = $this->orders()->sum('total');

        if ($sum < 10000) {
            return 1;
        } elseif ($sum < 30000) {
            return 2;
        } elseif ($sum < 50000) {
            return 3;
        } else {
            return 5;
        };
    }
}
