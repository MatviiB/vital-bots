<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public $timestamps = false;

    protected $fillable = ['client_id', 'address', 'floor', 'service_lift'];

    public static function updateAddresses($addresses, $client_id)
    {
        foreach ($addresses as $key => $address) {
            if ($key === 'new' && !empty($address['address'])) {
                self::create($addresses['new'] + ['client_id' => $client_id]);
            } elseif ($key !== 'new') {
                if (empty(trim($address['address']))) {
                    if (!Order::where('address_id', $key)->count()) {
                        self::where('id', $key)->delete();
                    }
                } else {
                    self::where('id', $key)->update($address);
                }
            }
        }
    }
}
