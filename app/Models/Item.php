<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    const STATUS_VERIFY   = 1;
    const STATUS_IN_WORK  = 2;
    const STATUS_TO_STORE = 3;
    const STATUS_ON_STORE = 4;
    const STATUS_DELIVERY = 5;
    const STATUS_SHIPPED  = 6;
    const STATUS_REJECTED = 7;

    protected $fillable = ['order_id', 'name', 'factory_id', 'factory_number',
        'status', 'description', 'price_in', 'price_out', 'lifting', 'assembly'];

    protected $appends = ['status_verbal'];

    public function factory()
    {
        return $this->belongsTo('App\Models\Factory');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function getStatusVerbalAttribute()
    {
        return self::statuses()[$this->status];
    }

    public function isNew()
    {
        if ($this->status === self::STATUS_VERIFY) {
            return true;
        }

        return false;
    }

    public function isStore()
    {
        if ($this->status === self::STATUS_ON_STORE) {
            return true;
        }

        return false;
    }

    public static function statuses() {
        return [
            self::STATUS_VERIFY   => 'verifying',
            self::STATUS_IN_WORK  => 'in work',
            self::STATUS_TO_STORE => 'to store',
            self::STATUS_ON_STORE => 'on store',
            self::STATUS_DELIVERY => 'delivery',
            self::STATUS_SHIPPED  => 'shipped',
//            self::STATUS_REJECTED => 'rejected'
        ];
    }

    public static function index(Request $request)
    {
        $items = self::query()->with('order', 'factory');

        if ($request->filled('order_id')) {
            $items->where('order_id', $request->get('order_id'));
        }

        if ($request->filled('status')) {
            $items->where('status', array_search(
                    str_replace('_', ' ', $request->get('status')),
                    Item::statuses())
            );
        }

        if ($request->filled('factory_id')) {
            $items->where('factory_id', $request->get('factory_id'));
        }

        return $items->latest()->paginate();
    }

    public static function report_1(Request $request)
    {
        $items = self::query()->with('order');

        if ($request->get('date_from')) {
            $items->whereHas('order', function($query) use ($request) {
                $query->whereDate('date_created', '>=', $request->get('date_from'));
            });
        } else {
            $items->whereHas('order', function($query) {
                $query->whereDate('date_created', '>=', Carbon::now()->startOfMonth());
            });
        }

        if ($request->get('date_to')) {
            $items->whereHas('order', function($query) use ($request) {
                $query->whereDate('date_created', '<=', $request->get('date_to'));
            });
        } else {
            $items->whereHas('order', function($query) {
                $query->whereDate('date_created', '<=', Carbon::now()->endOfMonth());
            });
        }

        if ($request->get('user_id')) {
            $items->whereHas('order', function($query) use ($request) {
                $query->where('user_id', $request->get('user_id'));
            });
        }

        $data = [];
        foreach ($items->get() as $key => $item) {
            if ($request->has('export')) {
                $data[$key]['IDs'] = $item->id . '-' . $item->order->id;
            } else {
                $data[$key]['IDs'] = '<nobr>' . $item->id . '-' . $item->order->id . '</nobr>';
            }
            $data[$key]['Name'] = $item->name;
            $data[$key]['Description'] = $item->description;
            $data[$key]['Price (IN)'] = $item->price_in;
            $data[$key]['Price (OUT)'] = $item->price_out;
            $data[$key]['Conversion'] = $item->order->conversion_status_verbal;
            $data[$key]['Date'] = $item->order->date_created->format('d/m/Y');
            $data[$key]['Status'] = $item->status_verbal;
        }

        return $data;
    }

    public static function report_3(Request $request)
    {
        $items = self::query();

        if ($request->get('date_from')) {
            $items->whereHas('order', function($query) use ($request) {
                $query->whereDate('date_created', '>=', $request->get('date_from'));
            });
        } else {
            $items->whereHas('order', function($query) {
                $query->whereDate('date_created', '>=', Carbon::now()->startOfMonth());
            });
        }

        if ($request->get('date_to')) {
            $items->whereHas('order', function($query) use ($request) {
                $query->whereDate('date_created', '<=', $request->get('date_to'));
            });
        } else {
            $items->whereHas('order', function($query) {
                $query->whereDate('date_created', '<=', Carbon::now()->endOfMonth());
            });
        }

        if ($request->get('factory_id')) {
            $items->where('factory_id', $request->get('factory_id'));
        }

        $data = [];
        foreach ($items->get() as $key => $item) {
            $data[$key]['IDs'] = $item->id;
            $data[$key]['Name'] = $item->name;
            $data[$key]['Description'] = $item->description;
            $data[$key]['Factory Number'] = $item->factory_number;
            $data[$key]['Price (IN)'] = $item->price_in;
            $data[$key]['Price (OUT)'] = $item->price_out;
            $data[$key]['Status'] = $item->status_verbal;
        }

        return $data;

        return $items->get();
    }
}
