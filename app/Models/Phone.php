<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = ['client_id', 'phone'];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        self::creating(function($model) {
            $model->phone = preg_replace('/\D/', '', $model->phone);
        });

        self::updating(function($model) {
            $model->phone = preg_replace('/\D/', '', $model->phone);
        });

        self::saving(function($model) {
            $model->phone = preg_replace('/\D/', '', $model->phone);
        });
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public static function updatePhones($phones, $client_id)
    {
        foreach ($phones as $id => $input) {
            if ($id !== 'new') {
                if (empty($input['phone'])) {
                    self::where('id', $id)->delete();
                } else {
                    self::where('id', $id)
                        ->update(['phone' => preg_replace('/\D/', '', $input['phone'])]);
                }
            } else {
                if (!empty($input['phone'])) {
                    self::create($input + ['client_id' => $client_id]);
                }
            }
        }
    }
}
