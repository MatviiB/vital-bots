<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Order extends Model
{
    //<editor-fold desc="Constants">
    const STATUS_VERIFY   = 1;
    const STATUS_IN_WORK  = 2;
    const STATUS_TO_STORE = 3;
    const STATUS_ON_STORE = 4;
    const STATUS_DELIVERY = 5;
    const STATUS_SHIPPED  = 6;

    const CONVERSION_ONLINE  = 1;
    const CONVERSION_SHOP    = 2;
    const CONVERSION_MEASURE = 3;

    const PREPAYMENT_TYPE_CASH = 1;
    const PREPAYMENT_TYPE_BILL = 2;

    const PREPAYMENT_STATUS_NOT_PASSED = 0;
    const PREPAYMENT_STATUS_PASSED     = 1;

    //</editor-fold>

    protected $fillable = ['client_id', 'status', 'address_id', 'total', 'delivery',
        'prepayment', 'prepayment_type', 'prepayment_status', 'discount', 'user_id',
        'note', 'tracklist_id', 'tracking_order', 'date_created', 'date_ready', 'bonus', 'bonus_percentage', 'conversion'];

    protected $appends = ['conversion_status_verbal', 'prepayment_type_verbal',
        'prepayment_status_verbal', 'status_verbal', 'date_ready_verbal', 'date_created_verbal'];

    protected $dates = ['date_created', 'date_ready'];

    //<editor-fold desc="Relations">

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }

    public function tracklist()
    {
        return $this->belongsTo('App\Models\Tracklist');
    }

    //</editor-fold>
    //<editor-fold desc="Accessors/Mutators">

    public function getDateReadyVerbalAttribute()
    {
        $date = Carbon::parse($this->date_ready);

        return [
            'date'  => $date->format('d'),
            'month' => $date->format('F'),
            'day'   => $date->format('l')
        ];
    }

    public function getDateCreatedVerbalAttribute()
    {
        $date = Carbon::parse($this->date_created);

        return [
            'date'  => $date->format('d'),
            'month' => $date->format('F'),
            'day'   => $date->format('l')
        ];
    }

    public function getStatusVerbalAttribute()
    {
        return self::statuses()[$this->status];
    }

    public function getConversionStatusVerbalAttribute()
    {
        return self::conversionStatuses()[$this->conversion];
    }

    public function getPrepaymentTypeVerbalAttribute()
    {
        return self::prepaymentTypes()[$this->prepayment_type];
    }

    public function getPrepaymentStatusVerbalAttribute()
    {
        return self::prepaymentStatuses()[$this->prepayment_status];
    }

    public function getBonusAttribute()
    {
        $sum = $this->client->orders()
            ->where('id', '<>', $this->id)
            ->sum('total');

        if ($sum < 10000) {
            $bonus = 1;
        } elseif ($sum < 30000) {
            $bonus = 2;
        } elseif ($sum < 50000) {
            $bonus = 3;
        } else {
            $bonus = 5;
        };

        return $bonus * floor($this->total / 100);
    }

    //</editor-fold>
    //<editor-fold desc="Scopes">

    public function scopeIsClose($query)
    {
        return $query->where('status', self::STATUS_CLOSED);
    }

    public function scopeIsNew($query)
    {
        return $query->where('status', self::STATUS_VERIFY);
    }

    //</editor-fold>
    //<editor-fold desc="Functions">

    public static function statuses()
    {
        return [
            self::STATUS_VERIFY   => 'verifying',
            self::STATUS_IN_WORK  => 'in work',
            self::STATUS_TO_STORE => 'to store',
            self::STATUS_ON_STORE => 'on store',
            self::STATUS_DELIVERY => 'delivery',
            self::STATUS_SHIPPED  => 'shipped'
        ];
    }

    public static function conversionStatuses()
    {
        return [
            self::CONVERSION_ONLINE  => 'online',
            self::CONVERSION_SHOP    => 'shop',
            self::CONVERSION_MEASURE => 'measure'
        ];
    }

    public static function prepaymentTypes()
    {
        return [
            self::PREPAYMENT_TYPE_CASH => 'cash',
            self::PREPAYMENT_TYPE_BILL => 'bill',
        ];
    }

    public static function prepaymentStatuses()
    {
        return [
            self::PREPAYMENT_STATUS_NOT_PASSED => 'not passed',
            self::PREPAYMENT_STATUS_PASSED     => 'passed'
        ];
    }

    public function updateStatus()
    {
        $statuses = $this->items()
            ->groupBy('status')
            ->pluck('status')
            ->toArray();

        if (count($statuses) > 1 && in_array(Item::STATUS_VERIFY, $statuses)) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_VERIFY;
            $this->save();
            return;
        }

        if (count($statuses) > 1 && in_array(Item::STATUS_IN_WORK, $statuses)) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_IN_WORK;
            $this->save();
            return;
        }

        if (count($statuses) > 1 && in_array(Item::STATUS_TO_STORE, $statuses)) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_TO_STORE;
            $this->save();
            return;
        }

        if (count($statuses) > 1 && in_array(Item::STATUS_ON_STORE, $statuses)) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_ON_STORE;
            $this->save();
            return;
        }

        if (count($statuses) === 1 && $statuses[0] === Item::STATUS_VERIFY) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_VERIFY;
            $this->save();
            return;
        }

        if (count($statuses) === 1 && $statuses[0] === Item::STATUS_IN_WORK) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_IN_WORK;
            $this->save();
            return;
        }

        if (count($statuses) === 1 && $statuses[0] === Item::STATUS_TO_STORE) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_TO_STORE;
            $this->save();
            return;
        }

        if (count($statuses) === 1 && $statuses[0] === Item::STATUS_ON_STORE) {
            $this->checkDowngradeFromShippedStatus();
            $this->removeFromTracklist();
            $this->status = self::STATUS_ON_STORE;
            $this->save();
            return;
        }

        if (count($statuses) === 1 && $statuses[0] === Item::STATUS_DELIVERY) {
            $this->checkDowngradeFromShippedStatus();
            $this->status = self::STATUS_DELIVERY;
            $this->save();
            return;
        }

        if (count($statuses) === 1 && $statuses[0] === Item::STATUS_SHIPPED) {
            if ($this->status !== self::STATUS_SHIPPED) {
                $this->sendBonus();
            }
            $this->status = self::STATUS_SHIPPED;
            $this->save();
            return;
        }
    }

    private function checkDowngradeFromShippedStatus()
    {
        if ($this->status === self::STATUS_SHIPPED) {
            $this->takeBonusBack();
        }
    }

    private function removeFromTracklist()
    {
        $this->tracklist_id = null;
    }


    public function sendBonus()
    {
        $this->client->discount += $this->bonus;
        $this->client->save();
    }

    public function takeBonusBack()
    {
        $this->client->discount -= $this->bonus;
        $this->client->save();
    }

    //</editor-fold>
    //<editor-fold desc="Front Requests">

    public static function index(Request $request)
    {
        $orders = self::with('user', 'client.phones', 'address', 'items', 'tracklist');

        if ($request->filled('id')) {
            $orders->where('id', $request->get('id'));
        }

        if ($request->filled('status')) {
            $orders->where('status', array_search(
                    str_replace('_', ' ', $request->get('status')),
                    self::statuses())
            );
        }

        if ($request->filled('tracklist')) {
            $orders->where('tracklist_id', $request->get('tracklist'))
                ->orderBy('tracking_order');
        }

        if ($request->has('filter')) {
            switch ($request->get('filter')) {
                case 'week':
                    $orders->whereBetween('date_ready', [
                        Carbon::now(), Carbon::now()->addWeek()
                    ])->orderBy('date_ready')
                        ->whereIn('status', [
                            self::STATUS_VERIFY,
                            self::STATUS_IN_WORK,
                            self::STATUS_ON_STORE
                        ]);
                    break;
                case 'overdue':
                    $orders->where('date_ready', '<', Carbon::now())
                        ->orderBy('date_ready')
                        ->whereIn('status', [
                            self::STATUS_VERIFY,
                            self::STATUS_IN_WORK,
                            self::STATUS_ON_STORE
                        ]);
                    break;
                default:
                    $orders->latest();
            }
        } else {
            $orders->latest();
        }

        if ($request->filled('phone')) {
            $ids = Phone::where('phone', preg_replace('/\D/', '', $request->get('phone')))
                ->pluck('client_id')->toArray();

            $orders->whereIn('client_id', $ids);
        }

        if ($request->has('client_id')) {
            $orders->where('client_id', $request->get('client_id'));
        }

        return $orders->paginate();
    }

    public static function report_2(Request $request)
    {
        $orders = self::query()->with('client.addresses', 'client.phones');

        if ($request->get('date_from')) {
            $orders->whereDate('date_created', '>=', $request->get('date_from'));
        } else {
            $orders->whereDate('date_created', '>=', Carbon::now()->startOfMonth());
        }

        if ($request->get('date_to')) {
            $orders->whereDate('date_created', '<=', $request->get('date_to'));
        } else {
            $orders->whereDate('date_created', '<=', Carbon::now()->endOfMonth());
        }

        $data = [];
        foreach ($orders->get() as $key => $order) {
            $data[$key]['ID'] = $order->id;
            $data[$key]['Name'] = $order->client->last_name . ' ' . $order->client->first_name;
            $data[$key]['Address'] = implode('<br>', $order->client->addresses->pluck('address')->toArray());
            $data[$key]['Phone'] = implode('<br>', $order->client->phones->pluck('phone')->toArray());
            $data[$key]['Total'] = $order->total;
            $data[$key]['Prepayment'] = $order->prepayment;
            $data[$key]['Prepayment Type'] = $order->prepayment_type_verbal;
            $data[$key]['Date'] = $order->date_created->format('d/m/Y');
        }

        return $data;
    }

    //</editor-fold>
}
