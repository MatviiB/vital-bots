<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tracklist extends Model
{
    protected $fillable = ['name', 'date', 'status'];

    protected $appends = ['date_track'];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function getDateTrackAttribute()
    {
        return Carbon::parse($this->date)->format('d/m');
    }
}
