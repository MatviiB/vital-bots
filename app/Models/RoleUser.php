<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'roles_users';

    protected $fillable = ['role_id', 'user_id'];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
