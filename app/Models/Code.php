<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $fillable = [
        'user_id',
        'vital_id',
        'code',
        'status'
    ];

    public static function genNew()
    {
        return random_int(111, 999) . ' ' . random_int(1111, 9999);
    }
}
