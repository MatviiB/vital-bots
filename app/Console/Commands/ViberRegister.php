<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Viber\Client;

class ViberRegister extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'viber:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $client = new Client(['token' =>  config('botman.viber.token')]);
            $result = $client->setWebhook('https://bots.vitalmebli.com.ua/viber');
            echo "Success!\n";
        } catch (\Exception $e) {
            echo "Error: ". $e->getMessage() ."\n";
        }
    }
}
