<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = Client::find(1);
        dd($client->bonus_percentage);

        $input = [
            'event' => 'message',
            'timestamp' => 1533196713729,
            'message_token' => 5205714010786660786,
            'sender' => [
                    'id' => 'p9A68prbd5aeDCdqpoTV7g==',
                    'name' => 'Matvii',
                    'language' => 'en',
                    'country' => 'UA',
                    'api_version' => 2,
                ],
            'message' => [
                    'text' => 'ping',
                    'type' => 'text',
                ],
            'silent' => false,
        ];


        $data = json_encode($input);
        $bitricks_token = '4572f44f897242ef-f7ec87637cf63cb2-75b569772005c00f';

        $sig = hash_hmac('sha256', $data, $bitricks_token);

        $url = 'https://im.bitrix.info/imwebhook/eh/050bd7328e1be26144ac95994857ba9a1505927215/?sig=' .
            $sig;

        $headers = [
//            'X-Viber-Auth-Token: ' . config('botman.viber.token'),
            'Content-Type: application/json',
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);


       /* $url = 'https://im.bitrix.info/imwebhook/eh/050bd7328e1be26144ac95994857ba9a1505927215/?sig=b2a862ec23d959e1c2491c4331731f939cfc7a2b5a923d4b5e5962a673b69459';

        $headers = [
//            'X-Viber-Auth-Token: ' . config('botman.viber.token'),
            'Content-Type: application/json',
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($input));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);*/
    }
}
