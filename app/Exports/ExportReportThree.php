<?php

namespace App\Exports;

use App\Models\Item;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportReportThree implements FromView
{
    public function view(): View
    {
        return view('reports.table', [
            'data' => Item::report_3(request())
        ]);
    }
}
