<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportReportTwo implements FromView
{
    public function view(): View
    {
        return view('reports.table', [
            'data' => Order::report_2(request())
        ]);
    }
}
