<?php

use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('ping', function ($bot) {
    $bot->reply('pong');
});

$botman->hears('/start', BotManController::class . '@start');

$botman->hears('код', BotManController::class . '@code');
$botman->hears('баланс', BotManController::class . '@balance');
$botman->hears('інфо', BotManController::class . '@info');



/*$botman->fallback(function($bot) {
    $url = 'https://im.bitrix.info/imwebhook/eh/362231febac535a1d5ad3d04e32ebfa11512199730/';
    $headers = [
        'Content-Type: application/json',
    ];
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(request()->all()));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    curl_close($ch);
});*/

//$botman->hears('subscribe', BotManController::class . '@subscribe');
//$botman->hears('unsubscribe', BotManController::class . '@unsubscribe');


//$botman->hears('Start conversation', BotManController::class.'@startConversation');
