<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

Route::middleware('auth')->group(function() {

    Route::get('/', function () {
        return view('dashboard');
    });

    Route::prefix('clients')->group(function () {
        Route::get('/', 'ClientsController@index')->name('clients.index');
        Route::get('edit/{id}', 'ClientsController@edit')->name('client.edit');
        Route::post('{id}', 'ClientsController@update')->name('client.update');
        Route::get('/create', 'ClientsController@create')->name('client.create');
        Route::post('/', 'ClientsController@store')->name('client.store');
        Route::delete('{id}', 'ClientsController@delete')->name('client.delete');
    });

    Route::prefix('orders')->group(function () {
        //Route::get('/', 'OrdersController@index')->name('orders.index');
        Route::get('/', function () {
            return view('orders.vue');
        })->name('orders.index');

        Route::get('create', 'OrdersController@create')->name('order.create');
        Route::post('/', 'OrdersController@store')->name('order.store');
        Route::get('edit/{id}', 'OrdersController@edit')->name('order.edit');
        Route::put('{id}', 'OrdersController@update')->name('order.update');
//        Route::delete('{id}', 'OrdersController@delete')->name('order.delete');
//        Route::post('status/{id}', 'OrdersController@status')->name('order.status');
//        Route::get('passed/{id}', 'OrdersController@passed')->name('order.passed');

        Route::prefix('ax')->group(function () {
            Route::get('/', 'OrdersAxController@index')->name('orders.index.ax');
            Route::delete('/{id}', 'OrdersAxController@delete')->name('order.delete.ax');
            Route::post('passed/{id}', 'OrdersAxController@passed')->name('order.passed.ax');
            Route::post('status/{id}', 'OrdersAxController@status')->name('order.status.ax');
            Route::post('tracklist', 'OrdersAxController@tracklist')->name('order.tracklist.ax');
            Route::post('tracklist-order', 'OrdersAxController@tracklistOrder')->name('order.tracklist-order.ax');
        });
    });

    Route::prefix('tracklists')->group(function () {
        Route::get('/', 'TrackingController@index')->name('tracklists.index');
        Route::get('create', 'TrackingController@create')->name('tracklist.create');
        Route::post('/', 'TrackingController@store')->name('tracklist.store');
        Route::get('edit/{id}', 'TrackingController@edit')->name('tracklist.edit');
        Route::post('{id}', 'TrackingController@update')->name('tracklist.update');
        Route::delete('{id}', 'TrackingController@delete')->name('tracklist.delete');

        Route::get('ax/list', 'TrackingController@axList')->name('tracklists.list.ax');
    });

    Route::prefix('items')->group(function () {
        Route::get('/', 'ItemsController@index')->name('items.index');
        Route::get('ax', 'ItemsController@axIndex')->name('orders.index.ax');
        Route::post('ax/status/{id}', 'ItemsController@axStatus')->name('item.status.ax');
        Route::post('ax/{id}', 'ItemsController@axUpdate')->name('item.update.ax');

//        Route::get('/php', 'ItemsController@index')->name('items.index');
//        Route::post('/', 'ItemsController@store')->name('items.store');
//        Route::post('status/{id}', 'ItemsController@status')->name('item.status');
//        Route::delete('{id}', 'OrdersController@delete')->name('order.delete');

    });

    Route::prefix('factories')->group(function () {
        Route::get('/', 'FactoriesController@index')->name('factories.index');
        Route::post('/', 'FactoriesController@store')->name('factory.store');
        Route::post('{id}', 'FactoriesController@update')->name('factory.update');
        Route::delete('{id}', 'FactoriesController@delete')->name('factory.delete');
    });

    Route::prefix('reports')->group(function () {
        Route::get('/1', 'ReportsController@report_1')->name('reports.1');
        Route::get('/2', 'ReportsController@report_2')->name('reports.2');
        Route::get('/3', 'ReportsController@report_3')->name('reports.3');
    });

    Route::prefix('texts')->group(function () {
        Route::get('/', 'TextsController@texts')->name('texts.index');
        Route::post('/', 'TextsController@update')->name('texts.update');
    });

    Route::prefix('users')->group(function () {
        Route::get('/', 'UsersController@index')->name('users.index');
        Route::get('login/{id}', 'UsersController@login')->name('user.login');
    });

    Route::get('/botman/tinker', 'BotManController@tinker');
});

Route::any('viber/webhook', 'ViberController@webhook');
Route::get('viber/register', 'ViberController@register');


Route::match(['get', 'post'], '/botman', 'BotManController@handle');

Route::get('barcode/{code}.png', function ($code) {
    return DNS1D::getBarcodePNG($code, "EAN5", 5, 50);
})->name('barcode');

Route::get('test', function () {

    dd(Auth::user()->role);
//    dd('asd');
//    dd(route('barcode', str_replace(' ', '', 'asdf 34523')));
//    dd(route('texts'));


    echo env('APP_URL') .  DNS1D::getBarcodePNGPath(str_replace(' ', '', "444 5645"), "EAN5", 5, 50);
});





// VUE
//Route::get('vue', 'ExampleController@getVue');
//Route::get('vue/users','ExampleController@getUsersVue');
// END VUE

//Route::get('users', 'ExampleController@getUsers')->name('users');
//Route::post('users/unsubscribe', 'ExampleController@postUsersUnsubscribe');
//Route::delete('users/delete/{tel_id}', 'ExampleController@deleteUser');

//Route::post('message', '\@postMessage')->name('message');