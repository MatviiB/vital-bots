<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderConversionField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('conversion')->after('bonus')->default(1);
            $table->integer('prepayment_type')
                ->after('prepayment')
                ->default(1);
            $table->integer('prepayment_status')
                ->after('prepayment_type')
                ->default(0);
        });

        Schema::table('clients', function(Blueprint $table) {
            $table->dropColumn('phone');
        });

        Schema::create('phones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->string('phone');
            $table->index('client_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('conversion');
            $table->dropColumn('prepayment_type');
            $table->dropColumn('prepayment_status');
        });

        Schema::table('clients', function(Blueprint $table) {
            $table->string('phone')->after('last_name')->nullable();
        });

        Schema::drop('phones');
    }
}
