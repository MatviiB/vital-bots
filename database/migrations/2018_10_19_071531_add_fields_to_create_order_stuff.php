<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCreateOrderStuff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('delivery')->after('address_id')->default(0);
            $table
                ->dateTime('date_created')
                ->after('note')
                ->default(DB::raw('CURRENT_TIMESTAMP'));
        });

        Schema::table('items', function (Blueprint $table) {
            $table->text('description')->after('status')->nullable();
            $table->integer('lifting')->after('price_out')->default(0);
            $table->integer('assembly')->after('lifting')->default(0);
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->integer('floor')->after('address')->nullable();
            $table->tinyInteger('service_lift')->after('floor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('delivery');
            $table->dropColumn('date_created');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('lifting');
            $table->dropColumn('assembly');
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('floor');
            $table->dropColumn('service_lift');
        });
    }
}
