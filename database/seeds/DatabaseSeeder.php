<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'manager_1',
            'email' => 'manager_1@gmail.com',
            'password' => bcrypt('manager1pass'),
        ]);

        $user->role_user()->insert(['role_id' => 2, 'user_id' => $user->id]);

        $user = User::create([
            'name' => 'manager_2',
            'email' => 'manager_2@gmail.com',
            'password' => bcrypt('manager22pass'),
        ]);

        $user->role_user()->insert(['role_id' => 2, 'user_id' => $user->id]);

        $user = User::create([
            'name' => 'manager_3',
            'email' => 'manager_3@gmail.com',
            'password' => bcrypt('manager333pass'),
        ]);

        $user->role_user()->insert(['role_id' => 2, 'user_id' => $user->id]);

        $user = User::create([
            'name' => 'manager_4',
            'email' => 'manager_4@gmail.com',
            'password' => bcrypt('manager4444pass'),
        ]);

        $user->role_user()->insert(['role_id' => 2, 'user_id' => $user->id]);
    }
}
